/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0509OrdenaArraylist;

/**
 *
 * @author paco
 */
public class Persona {

    private String nombre;
    private String apellido1;
    private String apellido2;
    private String DNI;

    public Persona() {
        setNombre("");
        setApellido1("");
        setApellido2("");
        setDNI("");
    }

    public Persona(String DNI_, String nombre_, String apellido1_,
        String apellido2_) {
        setDNI(DNI_);
        setNombre(nombre_);
        setApellido1(apellido1_);
        setApellido2(apellido2_);

    }

    public Persona(String nombre_, String apellido1_, String apellido2_) {
        setNombre(nombre_);
        setApellido1(apellido1_);
        setApellido2(apellido2_);
        setDNI("");
    }

    public void setDNI(String DNI_) {
        DNI = DNI_;
    }

    public String toString() {
        return getDNI() + " " + getNombre() + " " + getApellido1() + " "
            + getApellido2();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDNI() {
        return DNI;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }
}
