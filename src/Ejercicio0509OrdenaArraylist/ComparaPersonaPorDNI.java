/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0509OrdenaArraylist;

import java.util.Comparator;

/**
 *
 * @author paco
 */
public class ComparaPersonaPorDNI implements Comparator<Persona> {

    public int compare(Persona p1, Persona p2) {
        return p1.getDNI().compareToIgnoreCase(p2.getDNI());
    }
}
