/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0509OrdenaArraylist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    List<Persona> agenda;

    public static void mostrarAgenda(List<Persona> agenda) {
        for (Persona persona : agenda) {
            System.out.println("" + persona);
        }
    }

    public static void main(String[] args) {

        List<Persona> agenda = new ArrayList<Persona>();

        agenda.add(new Persona("34563782G", "Juan", "Perez", "Perez"));
        agenda.add(new Persona("13784267H", "Fermin", "Lopez", "Anechina"));
        agenda.add(new Persona("12986512J", "Christian", "Alvarez", "Correa"));
        agenda.add(new Persona("22669123O", "Jesus", "Lopez", "Gimeno"));

        System.out.println("Agenda sin ordenar");
        mostrarAgenda(agenda);
        Collections.sort(agenda, new ComparaPersonaPorDNI());
        System.out.println("Agenda ordenada por DNI");
        mostrarAgenda(agenda);

    }

}
/* Ejecucion:
 Agenda sin ordenar
 34563782G Juan Perez Perez
 13784267H Fermin Lopez Anechina
 12986512J Christian Alvarez Correa
 22669123O Jesus Lopez Gimeno
 Agenda ordenada por DNI
 12986512J Christian Alvarez Correa
 13784267H Fermin Lopez Anechina
 22669123O Jesus Lopez Gimeno
 34563782G Juan Perez Perez
 */
