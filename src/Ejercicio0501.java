/**
 * Fichero: Ejercicio0501.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 17-ene-2014
 */
public class Ejercicio0501 {

  private final int TALLA = 15;
  private final char VACIO = '.';
  private char[][] tabla = new char[TALLA][TALLA];
  String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  String palabras[] = {"PACO", "JUAN", "ANTONIO", "GLADYS", "LAURA", "RODOLFO", "FLOR", "DIEGO", "FEDERICO", "DOLORES", "MANUEL", "RAMONA", "ROSANA", "FIDEL", "ISABEL"
  };
  // Posicion de la palabra a colocar
  int fila;
  int columna;
  char orientacion;

  /* ***********************************
   * Rellana la tabla con caracter vacio
   *************************************/
  public void crear() {
    for (int i = 0; i < TALLA; i++) {
      for (int j = 0; j < TALLA; j++) {
        tabla[i][j] = VACIO;
      }
    }
  }

  /* *****************************
   * Comprueba si la palabra p se puede
   * poner en la tabla.
   * Comprueba limites maximos y
   * que la celda esté ya ocupa
   ********************************/
  public boolean cabe(String p) {
    boolean cab = true, ocupado = false;
    int f = fila, c = columna, i;
    // Comprueba Limites = cabe
    if (orientacion == 'h' && p.length() + columna > TALLA) {
      cab = false;
    }
    if (orientacion == 'v' && p.length() + fila > TALLA) {
      cab = false;
    }
    // Solapa con otras palabras = ocupado
    if (orientacion == 'h' && cab) {
      i = 0;
      while (i < p.length() && !ocupado) {
        if (tabla[f][c] != VACIO) {
          ocupado = true;
        }
        if (ocupado == false) {
          c++;
        }
        i++;
      }
    }
    if (orientacion == 'v' && cab) {
      i = 0;
      while (i < p.length() && !ocupado) {
        if (tabla[f][c] != VACIO) {
          ocupado = true;
        }
        if (!ocupado) {
          f++; // cambia la fila en vertical
        }
        i++;
      }
    } // while
    //traza(p, ocupado, cab); // Descomentar para ver traza
    if (ocupado == true) {
      return cab = false;
    }
    return cab;
  }

  /* *****************************
   * Permite ver las operaciones
   * Descomentar linea 87
   * ******************************/
  public void traza(String p, boolean ocupado, boolean cab) {
    System.out.print(p + " en [" + fila + "," + columna + "]");
    System.out.print(" talla " + TALLA + " ");
    System.out.print(" orienta " + orientacion + " ");
    if (orientacion == 'h') {
      System.out.print(" lenth()+col " + (p.length() + columna) + " ");
    } else {
      System.out.print(" lenth()+fil " + (p.length() + fila) + " ");
    }
    if (ocupado) {
      System.out.print(" ocupado ");
    } else {
      System.out.print(" no ocupado ");
    }
    if (cab) {
      System.out.println(" cabe ");
    } else {
      System.out.println(" no cabe ");
    }
  }

  /* *****************************
   * Calcula fila, columna y ubicacion de
   * forma aleatoria
   * ***********************************/
  public void ubicacion() {
    int numo = 0;
    // donde ponerla
    //p = (int) (Math.random() * palabras.length); // palabra
    fila = (int) (Math.random() * TALLA);  // fila
    columna = (int) (Math.random() * TALLA);  // columna
    numo = (int) (Math.random() * 2); // 0=horiz, 1=vert
    if (numo == 1) {
      orientacion = 'h';
    } else {
      orientacion = 'v';
    }
  }

  /* *********************************************
   * Coloca la palabra en fila,columna,orientacion
   ************************************************/
  public void colocar(String p) {
    int cont = 0;
    // Horizontal
    if (orientacion == 'h') {
      cont = 0;
      for (int i = columna; cont < p.length(); i++, cont++) {
        tabla[fila][i] = p.charAt(cont);
      }
    } else { // o=='v'
      cont = 0;
      for (int i = fila; cont < p.length(); i++, cont++) {
        tabla[i][columna] = p.charAt(cont);
      }
    }
  }

  /* *****************************
   * Selecciona palabras, selecciona
   * donde ponerla, y la coloca.
   * ******************************/
  public void escoger() {
    int s, p = 0, f, c, cont;
    int fil, col;
    while (p < palabras.length) {
      ubicacion();
      while (cabe(palabras[p]) == false) {
        ubicacion();
      }
      colocar(palabras[p]);
      p++;
    }
  }

  /* *****************************
   * Rellena el caracter vacio con
   * una letra aleatoria
   * *****************************/
  public void rellenar() {
    int r;
    for (int i = 0; i < TALLA; i++) {
      for (int j = 0; j < TALLA; j++) {
        if (tabla[i][j] == VACIO) {
          r = (int) (Math.random() * letras.length());
          tabla[i][j] = letras.charAt(r);
        }
      }
    }
  }
  /* *************************************
   * Muestra las palabras de la sopa de letras
   * *************************************/

  private void mostrarpalabras() {
    int i = 1;
    for (String s : palabras) {
      System.out.println(i + " " + s);
      i++;
    }
  }
  /* *************************************
   * Muestra la tabla de la sopa de letras
   * *************************************/

  public void mostrar() {
    for (int i = 0; i < TALLA; i++) {
      for (int j = 0; j < TALLA; j++) {
        System.out.print(tabla[i][j] + " ");
      }
      System.out.println();
    }
  }
  /* *****************************
   *   main
   ********************************/

  public static void main(String args[]) {
    Ejercicio0501 s = new Ejercicio0501();
    s.crear();
    s.escoger();
    //s.rellenar(); // Comentar para ver palabras
    s.mostrarpalabras();
    s.mostrar();
  }
}
/* EJECUCION:
 1 PACO
 2 JUAN
 3 ANTONIO
 4 GLADYS
 5 LAURA
 6 RODOLFO
 7 FLOR
 8 DIEGO
 9 FEDERICO
 10 DOLORES
 11 MANUEL
 12 RAMONA
 13 ROSANA
 14 FIDEL
 15 ISABEL
 U F Q B V E O A D S C O D Q Q
 P D R O P R Z W K A J J N V J
 F R D T U H P I H A V K B M S
 O N Z U D W F L A U R A O Y C
 N U U S A Z X W K J A S Z W Z
 C I R A L C V Y H S D J U A N
 Y A O O M Z Q X A P A P Y D I
 L V D D P K X X K G L A D Y S
 K M O R W J F Y H E O M L V M
 G B L U L X Z B P O I U C J Z
 Z D F Z E Z M A N T O N I O A
 S D O P B F D I E G O N I T L
 M V Z A T P S Q E L S C D Q H
 Y J A C G N Z G R I T U B O M
 H K M O F L O R O S Q A K W Q
 */
