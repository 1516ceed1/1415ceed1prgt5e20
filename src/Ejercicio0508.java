
/**
 * @date 28-ene-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejercicio0508 {

  public static void main(String args[]) {
      
    final int tiempo =   10000;
      
    String s = new String();
    long inicio = System.currentTimeMillis();
    for (int i = 0; i < tiempo; i++) {
      s = s + "mitexto";
    }
    long fin = System.currentTimeMillis();
    System.out.println("Tiempo del String: " + (fin - inicio));
     
      
      
    StringBuffer sbuffer = new StringBuffer();
    inicio = System.currentTimeMillis();
    for (int i = 0; i < tiempo; i++) {
      sbuffer.append("mitexto");
    }
    fin = System.currentTimeMillis();
    System.out.println("Tiempo del StringBuffer: " + (fin - inicio));

    StringBuilder sbuilder = new StringBuilder();
    inicio = System.currentTimeMillis();
    for (int i = 0; i < tiempo; i++) {
      sbuilder.append("mitexto");
    }
    fin = System.currentTimeMillis();
    System.out.println("Tiempo del StringBuilder: " + (fin - inicio));

  }
}
/* EJECUCION:
 Tiempo del StringBuffer: 63
 Tiempo del StringBuilder: 28
 */
