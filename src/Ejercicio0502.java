/**
 * Fichero: Ejercicio0502.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 17-ene-2014
 */
public class Ejercicio0502 {

  public static boolean esCapicua(int dato) {
    Integer i = new Integer(dato);
    String reverse = new StringBuffer(i.toString()).reverse().toString();
    return i.toString().equals(reverse.toString()); // corregido
    // return i.toString().equals(reverse); // otra solucion
  }

  public static void main(String[] args) {
    System.out.println(esCapicua(121));
  }
}

/* EJECUCION:
 true
 */
