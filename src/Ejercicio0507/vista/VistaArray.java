/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0507.vista;

import java.io.IOException;
import modelo.Cliente;
import util.Util;

/**
 * Fichero: VistaArray.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-ene-2014
 */
public class VistaArray {

  public void getArticulos(Cliente ClientesArray[]) throws IOException {

    Vista vista = new Vista();
    Util util = new Util();
    String nombre, email;
    int edad;
    int correcto;

    System.out.println("VISTA ARRAY");
    System.out.println("TOMA DE DATOS");

    for (int i = 0; i < ClientesArray.length; i++) {

      nombre = "";
      email = "";
      edad = 0;

      System.out.print("Nombre: ");
      nombre = util.pedirString();

      while (edad <= 0) {
        System.out.print("Edad: ");
        edad = util.pedirInt();
        if (edad <= 0) {
          vista.error(2);
        }
      }

      correcto = 1;
      while (correcto == 1) {
        System.out.print("Email: ");
        email = util.pedirString();
        if (util.invalidoEmail(email) == true) {
          correcto = 0;
        } else {
          correcto = 1;
          vista.error(3);
        }
      }

      ClientesArray[i] = new Cliente(nombre, edad, email);
    }
  }

  public void showArticulos(Cliente ClientesArray[]) {

    System.out.println("VISTA ARRAY");
    System.out.println("MOSTRANDO DATOS");
    for (int i = 0; i < ClientesArray.length; i++) {
      System.out.println(i + ". " + ClientesArray[i].toString());
    }

  }
}
