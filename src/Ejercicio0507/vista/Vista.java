package Ejercicio0507.vista;

import java.io.IOException;
import util.Errores;
import util.Util;

/**
 * Fichero: Vista.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-nov-2013
 */
public class Vista {

  public int opcionO() throws IOException {
    int op;
    Util util;
    util = new Util();
    System.out.println("OPERACIONES");
    System.out.println("0. Fin. ");
    System.out.println("1. Añadir. ");
    System.out.println("2. Listar. ");
    System.out.print("Opción?: ");
    op = util.pedirInt();
    return op;
  }

  public int opcionE() throws IOException {
    int op;
    Util util;
    util = new Util();
    System.out.println("ESTRUCTURA");
    System.out.println("0. Fin. ");
    System.out.println("1. Array. ");
    System.out.println("2. ArrayList. ");
    System.out.print("Opción?: ");
    op = util.pedirInt();
    return op;
  }

  public void error(int i) {
    Errores error = new Errores();
    String t;
    t = error.tipo(i);
    System.out.println("Error: " + t);
  }

  public void fin() {
    System.out.println("Fin");
  }
}
