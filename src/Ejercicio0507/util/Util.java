package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Práctica Obligatoria Tema 3
 *
 * @author Paco Aldarias Raya
 * @date 18/10/2013
 */
public class Util {

  public int pedirInt() throws IOException {

    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    String linea;
    int numero;
    linea = buffer.readLine();
    numero = Integer.parseInt(linea);

    return numero;
  }

  public String pedirString() {

    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    String linea = null;
    try {
      linea = buffer.readLine();
    } catch (Exception e) {
    }
    return linea;
  }

  public boolean invalidoEmail(String email) {

    final String PATRON_EMAIL = "[-\\w\\.]+@[\\.\\w]+\\.\\w+";
    Pattern patron = Pattern.compile(PATRON_EMAIL);
    Matcher compara = patron.matcher(email);
    return compara.matches();

  }
}
